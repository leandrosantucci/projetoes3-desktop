package projetoestacionamento.janelas;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import com.sun.glass.events.KeyEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import modal.Conversao64;
import modal.Motorista;

public class MotoristaGUI extends javax.swing.JPanel {

    private boolean webAtivada = false;

    Color blueNormal = new Color(0, 102, 255);
    Color blueHover = new Color(0, 140, 255);
    boolean fotoTirada = false;

    JDialog janelaSelcArquivo;

    public MotoristaGUI() {
        initComponents();
    }

    public MotoristaGUI(Motorista moto) {
        initComponents();
        // Inicializando motorista conforme a referencia dos parâmetros
        ftxtRG.setText(moto.getRg());
        ftxtRG.setEnabled(false);
        txtNomeSobrenome.setText(moto.getNomeCompleto());
        lblFotoCadastro.setIcon(Conversao64.base64toImageIcon(moto.getFoto64(),
                lblFotoCadastro.getPreferredSize().width,
                lblFotoCadastro.getPreferredSize().height));
        System.out.println(moto.getSexo() + " = " + cbSexo.getItemAt(1));
        if (moto.getSexo().equals(cbSexo.getItemAt(1))) {
            cbSexo.setSelectedIndex(1);
        }
        jTextField1.setText(moto.getTelPrincipal());
        jTextField2.setText(moto.getTelRecados());
        motorista = moto;
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btnCarregarArquivo = new javax.swing.JLabel();
        jPanelCampos = new javax.swing.JPanel();
        jLabel20 = new javax.swing.JLabel();
        jlblNomeSobrenome = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        ftxtRG = new javax.swing.JFormattedTextField();
        txtNomeSobrenome = new javax.swing.JTextField();
        cbSexo = new javax.swing.JComboBox<>();
        jLabel24 = new javax.swing.JLabel();
        jlblNomeSobrenome1 = new javax.swing.JLabel();
        jTextField1 = new javax.swing.JTextField();
        jLabel25 = new javax.swing.JLabel();
        jTextField2 = new javax.swing.JTextField();
        webcamPanel = new javax.swing.JPanel();
        lblFotoCadastro = new javax.swing.JLabel();
        btnFinalizar = new javax.swing.JLabel();

        setBackground(new java.awt.Color(238, 238, 238));
        setForeground(new java.awt.Color(204, 204, 204));
        setMaximumSize(new java.awt.Dimension(810, 278));
        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
                formAncestorRemoved(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCarregarArquivo.setBackground(new java.awt.Color(102, 102, 102));
        btnCarregarArquivo.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        btnCarregarArquivo.setForeground(new java.awt.Color(204, 204, 204));
        btnCarregarArquivo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnCarregarArquivo.setText("CARREGAR ARQUIVO...");
        btnCarregarArquivo.setToolTipText("SOMENTE IMAGENS 200x200 EM FORMATO PNG");
        btnCarregarArquivo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnCarregarArquivo.setOpaque(true);
        btnCarregarArquivo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnCarregarArquivoMouseClicked(evt);
            }
        });
        add(btnCarregarArquivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 200, 20));

        jPanelCampos.setBackground(new java.awt.Color(235, 235, 235));
        jPanelCampos.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(223, 223, 223), 1, true));
        jPanelCampos.setOpaque(false);
        jPanelCampos.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel20.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(133, 133, 133));
        jLabel20.setText("SEXO(*)");
        jPanelCampos.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 10, -1, -1));

        jlblNomeSobrenome.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jlblNomeSobrenome.setText("(*) CAMPOS OBRIGATÓRIOS");
        jPanelCampos.add(jlblNomeSobrenome, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, 560, -1));

        jLabel23.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(133, 133, 133));
        jLabel23.setText("RG(*)");
        jPanelCampos.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        ftxtRG.setForeground(new java.awt.Color(0, 102, 153));
        try {
            ftxtRG.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##.###.###-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        ftxtRG.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                ftxtRGKeyPressed(evt);
            }
        });
        jPanelCampos.add(ftxtRG, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 110, -1));

        txtNomeSobrenome.setForeground(new java.awt.Color(0, 102, 153));
        txtNomeSobrenome.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNomeSobrenomeKeyTyped(evt);
            }
        });
        jPanelCampos.add(txtNomeSobrenome, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 30, 310, -1));

        cbSexo.setFont(new java.awt.Font("Dialog", 0, 12)); // NOI18N
        cbSexo.setForeground(new java.awt.Color(0, 102, 255));
        cbSexo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Feminino", "Masculino" }));
        jPanelCampos.add(cbSexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 30, 120, -1));

        jLabel24.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(133, 133, 133));
        jLabel24.setText("TELEFONE");
        jPanelCampos.add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 80, -1, -1));

        jlblNomeSobrenome1.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome1.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome1.setText("NOME COMPLETO(*)");
        jPanelCampos.add(jlblNomeSobrenome1, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, -1, -1));

        jTextField1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField1KeyTyped(evt);
            }
        });
        jPanelCampos.add(jTextField1, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 170, -1));

        jLabel25.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel25.setForeground(new java.awt.Color(133, 133, 133));
        jLabel25.setText("CELULAR");
        jPanelCampos.add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 80, -1, -1));

        jTextField2.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                jTextField2KeyTyped(evt);
            }
        });
        jPanelCampos.add(jTextField2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 100, 170, -1));

        add(jPanelCampos, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 20, 580, 200));

        webcamPanel.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        webcamPanel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                webcamPanelMouseClicked(evt);
            }
        });
        webcamPanel.setLayout(new javax.swing.OverlayLayout(webcamPanel));

        lblFotoCadastro.setBackground(new java.awt.Color(226, 226, 226));
        lblFotoCadastro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lblFotoCadastro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/user128.png"))); // NOI18N
        lblFotoCadastro.setAlignmentX(0.5F);
        lblFotoCadastro.setMaximumSize(new java.awt.Dimension(200, 200));
        lblFotoCadastro.setMinimumSize(new java.awt.Dimension(200, 200));
        lblFotoCadastro.setOpaque(true);
        lblFotoCadastro.setPreferredSize(new java.awt.Dimension(200, 200));
        webcamPanel.add(lblFotoCadastro);

        add(webcamPanel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 20, 200, 200));

        btnFinalizar.setBackground(new java.awt.Color(0, 102, 255));
        btnFinalizar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnFinalizar.setForeground(new java.awt.Color(204, 204, 204));
        btnFinalizar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnFinalizar.setText("FINALIZAR");
        btnFinalizar.setAlignmentX(0.5F);
        btnFinalizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFinalizar.setOpaque(true);
        btnFinalizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnFinalizarMouseClicked(evt);
            }
        });
        add(btnFinalizar, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 230, 208, 48));
    }// </editor-fold>//GEN-END:initComponents

    private void webcamPanelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_webcamPanelMouseClicked
        if (panel == null) {
            webcam = Webcam.getDefault();
            webcam.setViewSize(WebcamResolution.VGA.getSize());

            panel = new WebcamPanel(webcam);

            webcamPanel.add(panel, 1);
            webcamPanel.revalidate();
            webcamPanel.repaint();
            webcamPanel.setSize(new Dimension(200, 200));
            panel.setVisible(true);

        }

        if (webAtivada == true) {
            BufferedImage image = webcam.getImage();
            Dimension dm = WebcamResolution.VGA.getSize();
            int tamanho = 400;
            image = image.getSubimage((dm.width - tamanho) / 2, (dm.height - tamanho) / 2, tamanho, tamanho);
            try {
                ImageIO.write(image, "PNG", new File("temp.png"));
            } catch (IOException ex) {
                Logger.getLogger(MotoristaGUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            lblFotoCadastro.setIcon(null);

            try {
                lblFotoCadastro.setIcon(new ImageIcon(ImageIO.read(new File("temp.png")).getScaledInstance(200, 200, java.awt.Image.SCALE_SMOOTH)));
            } catch (IOException ex) {
                Logger.getLogger(MotoristaGUI.class.getName()).log(Level.SEVERE, null, ex);
            }

            fotoTirada = true;
            lblFotoCadastro.setVisible(true);
            webAtivada = false;
            panel.setVisible(false);
            panel.stop();
            btnCarregarArquivo.setVisible(true);
        } else {
            btnCarregarArquivo.setVisible(false);
            panel.setVisible(true);
            lblFotoCadastro.setVisible(false);
            webAtivada = true;
            panel.start();
        }
    }//GEN-LAST:event_webcamPanelMouseClicked

    private void formAncestorRemoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorRemoved
        if (panel != null) {
            System.out.println("Webcam Desativada");
            panel.stop();
            try {
                new File("temp.png").delete();
            } catch (Exception e) {
                System.out.println(e);
            }
        }
    }//GEN-LAST:event_formAncestorRemoved

    private void btnCarregarArquivoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnCarregarArquivoMouseClicked
        janelaSelcArquivo = null;
        janelaSelcArquivo = new JDialog();
        janelaSelcArquivo.setTitle("Selecionar Imagem");
        janelaSelcArquivo.setModal(true);
        janelaSelcArquivo.setLocationRelativeTo(this);
        JFileChooser fc = new JFileChooser();

        //PEGAR TODO TIPO DE IMAGEM
        /*FileFilter imageFilter = new FileNameExtensionFilter(
                "Arquivos de imagem", ImageIO.getReaderFileSuffixes());*/
        //ACEITAR SOMENTE IMAGEM DO FORMATO PNG
        FileFilter imageFilter = new FileNameExtensionFilter(
                "Arquivos PNG", "png");

        janelaSelcArquivo.getContentPane().add(fc, BorderLayout.CENTER);
        fc.setFileFilter(imageFilter);
        fc.setAcceptAllFileFilterUsed(false);
        fc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (e.getActionCommand() == null ? JFileChooser.APPROVE_SELECTION == null : e.getActionCommand().equals(JFileChooser.APPROVE_SELECTION)) {
                    File selectedFile = fc.getSelectedFile();
                    System.out.println(selectedFile.getParent());
                    System.out.println(selectedFile.getName());
                    janelaSelcArquivo.dispose();
                    BufferedImage image;
                    try {
                        image = ImageIO.read(selectedFile);
                        ImageIO.write(image, "PNG", new File("temp.png"));
                    } catch (IOException ex) {
                        Logger.getLogger(MotoristaGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    try {
                        lblFotoCadastro.setIcon(new ImageIcon(ImageIO.read(new File("temp.png"))));
                        fotoTirada = true;
                    } catch (IOException ex) {
                        Logger.getLogger(MotoristaGUI.class.getName()).log(Level.SEVERE, null, ex);
                    }

                }
                if (e.getActionCommand() == JFileChooser.CANCEL_SELECTION) {
                    janelaSelcArquivo.dispose();
                }
            }
        });
        janelaSelcArquivo.pack();
        janelaSelcArquivo.setLocationRelativeTo(null);
        janelaSelcArquivo.setVisible(true);
    }//GEN-LAST:event_btnCarregarArquivoMouseClicked

    private void btnFinalizarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFinalizarMouseClicked
        FinalizarCadastro();
    }//GEN-LAST:event_btnFinalizarMouseClicked

    private void ftxtRGKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_ftxtRGKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            FinalizarCadastro();
        }
    }//GEN-LAST:event_ftxtRGKeyPressed

    private void jTextField1KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField1KeyTyped
        char enter = evt.getKeyChar(); //015981483884
        if (!(Character.isDigit(enter)) || jTextField1.getText().length() > 11) {
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            FinalizarCadastro();
        }
    }//GEN-LAST:event_jTextField1KeyTyped

    private void jTextField2KeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTextField2KeyTyped
        char enter = evt.getKeyChar();
        if (!(Character.isDigit(enter)) || jTextField2.getText().length() > 11) {
            evt.consume();
        }
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            FinalizarCadastro();
        }
    }//GEN-LAST:event_jTextField2KeyTyped

    private void txtNomeSobrenomeKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNomeSobrenomeKeyTyped
        char enter = evt.getKeyChar();
        if ((!Character.isLetter(enter) && !Character.isWhitespace(enter)) || txtNomeSobrenome.getText().length() > 49) {
            evt.consume();
        }

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            FinalizarCadastro();
        }
    }//GEN-LAST:event_txtNomeSobrenomeKeyTyped

    private void FinalizarCadastro() {
        System.out.println(ftxtRG.isEnabled());
        if (ftxtRG.isEnabled()) {
            motorista = new Motorista();
            if (webAtivada == false && fotoTirada == true) {
                motorista.setFoto64(Conversao64.ImagePNGtoBase64(new File("temp.png")));
                motorista.setRg(ftxtRG.getText());
                motorista.setNomeCompleto(txtNomeSobrenome.getText());
                motorista.setSexo(cbSexo.getItemAt(cbSexo.getSelectedIndex()));

                if (jTextField1.getText().isEmpty()) {
                    motorista.setTelPrincipal(null);
                } else {
                    motorista.setTelPrincipal(jTextField1.getText());
                }

                if (jTextField2.getText().isEmpty()) {
                    motorista.setTelRecados(null);
                } else {
                    motorista.setTelRecados(jTextField2.getText());
                }

                switch (motorista.salvar()) {
                    case 0:
                        JOptionPane.showConfirmDialog(null, "Cadastro realizado com sucesso!", "Cadastro de Motorista", JOptionPane.CLOSED_OPTION);
                        Component[] componentes = jPanelCampos.getComponents();
                        for (Component cmp : componentes) {
                            if (cmp instanceof JTextField) {
                                ((JTextField) cmp).setText("");
                            }
                            if (cmp instanceof JFormattedTextField) {
                                ((JFormattedTextField) cmp).setText("");
                            }
                        }
                        ftxtRG.requestFocus();
                        lblFotoCadastro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/user128.png")));
                        fotoTirada = false;
                        try {
                            new File("temp.png").delete();
                        } catch (Exception e) {
                            System.out.println(e);
                        }
                        break;
                    case 1:
                        JOptionPane.showConfirmDialog(null, "Preencha todos os campos corretamente.", "Cadastro de Motorista", JOptionPane.CLOSED_OPTION);
                        break;
                    case 2:
                        JOptionPane.showConfirmDialog(null, "Ocorreu um erro ao cadastrar no Banco.", "Cadastro de Motorista", JOptionPane.CLOSED_OPTION);
                        break;
                }
            } else {
                JOptionPane.showConfirmDialog(null, "É necessária a foto para identificação do motorista.", "Cadastro de Motorista", JOptionPane.CLOSED_OPTION);
            }
        } else {
            if (webAtivada == false && fotoTirada == true) {
                motorista.setFoto64(Conversao64.ImagePNGtoBase64(new File("temp.png")));
            }
            motorista.setNomeCompleto(txtNomeSobrenome.getText());
            motorista.setSexo(cbSexo.getItemAt(cbSexo.getSelectedIndex()));

            if (jTextField1.getText().isEmpty()) {
                motorista.setTelPrincipal(null);
            } else {
                motorista.setTelPrincipal(jTextField1.getText());
            }

            if (jTextField2.getText().isEmpty()) {
                motorista.setTelRecados(null);
            } else {
                motorista.setTelRecados(jTextField2.getText());
            }

            switch (motorista.update()) {
                case 0:
                    JOptionPane.showConfirmDialog(null, "Alteração realizada com sucesso!", "Alteração de motorista", JOptionPane.CLOSED_OPTION);
                    try {
                        new File("temp.png").delete();
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                    break;
                case 1:
                    JOptionPane.showConfirmDialog(null, "Preencha todos os campos corretamente.", "Alteração de motorista", JOptionPane.CLOSED_OPTION);
                    break;
                case 2:
                    JOptionPane.showConfirmDialog(null, "Ocorreu um erro ao alterar no Banco.", "Alteração de motorista", JOptionPane.CLOSED_OPTION);
                    break;
            }
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnCarregarArquivo;
    private javax.swing.JLabel btnFinalizar;
    private javax.swing.JComboBox<String> cbSexo;
    private javax.swing.JFormattedTextField ftxtRG;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JPanel jPanelCampos;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JLabel jlblNomeSobrenome;
    private javax.swing.JLabel jlblNomeSobrenome1;
    private javax.swing.JLabel lblFotoCadastro;
    private javax.swing.JTextField txtNomeSobrenome;
    private javax.swing.JPanel webcamPanel;
    // End of variables declaration//GEN-END:variables
    private WebcamPanel panel = null;
    Webcam webcam = null;
    private Motorista motorista;
}
