package projetoestacionamento.janelas;

import java.awt.Component;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import modal.Veiculo;

public class VeiculoGUI extends javax.swing.JPanel {

    private Veiculo veiculo;

    public VeiculoGUI() {
        initComponents();
    }

    public VeiculoGUI(Veiculo veiculo) {
        initComponents();

        this.txtCod.setText(String.valueOf(veiculo.getCodigo()));
        this.txtPlaca.setText(veiculo.getPlaca());
        this.txtMarca.setText(veiculo.getMarca());
        this.txtModelo.setText(veiculo.getModelo());
        this.txtCor.setText(veiculo.getCor());
        this.txtAdesivo.setText(veiculo.getAdesivo());

        this.veiculo = veiculo;

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanelCampos = new javax.swing.JPanel();
        txtPlaca = new javax.swing.JTextField();
        jlblNomeSobrenome1 = new javax.swing.JLabel();
        txtCod = new javax.swing.JTextField();
        txtCor = new javax.swing.JTextField();
        jlblNomeSobrenome3 = new javax.swing.JLabel();
        txtMarca = new javax.swing.JTextField();
        jlblNomeSobrenome4 = new javax.swing.JLabel();
        txtAdesivo = new javax.swing.JTextField();
        jlblNomeSobrenome5 = new javax.swing.JLabel();
        jlblNomeSobrenome6 = new javax.swing.JLabel();
        jlblNomeSobrenome2 = new javax.swing.JLabel();
        txtModelo = new javax.swing.JTextField();
        jlblNomeSobrenome8 = new javax.swing.JLabel();
        btnFinalizar = new javax.swing.JLabel();
        lblGeradoAuto = new javax.swing.JLabel();

        setBackground(new java.awt.Color(238, 238, 238));
        setForeground(new java.awt.Color(204, 204, 204));
        setMaximumSize(new java.awt.Dimension(810, 278));
        addAncestorListener(new javax.swing.event.AncestorListener() {
            public void ancestorAdded(javax.swing.event.AncestorEvent evt) {
            }
            public void ancestorRemoved(javax.swing.event.AncestorEvent evt) {
                formAncestorRemoved(evt);
            }
            public void ancestorMoved(javax.swing.event.AncestorEvent evt) {
            }
        });

        jPanelCampos.setBackground(new java.awt.Color(235, 235, 235));
        jPanelCampos.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(223, 223, 223), 1, true));
        jPanelCampos.setOpaque(false);
        jPanelCampos.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtPlaca.setForeground(new java.awt.Color(0, 102, 153));
        txtPlaca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPlacaKeyTyped(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtPlacaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtPlacaKeyReleased(evt);
            }
        });
        jPanelCampos.add(txtPlaca, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 30, 120, -1));

        jlblNomeSobrenome1.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome1.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome1.setText("Código");
        jPanelCampos.add(jlblNomeSobrenome1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, -1, -1));

        txtCod.setForeground(new java.awt.Color(0, 102, 153));
        txtCod.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtCod.setText("automatico");
        txtCod.setEnabled(false);
        jPanelCampos.add(txtCod, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, 90, -1));

        txtCor.setForeground(new java.awt.Color(0, 102, 153));
        txtCor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCorKeyTyped(evt);
            }
        });
        jPanelCampos.add(txtCor, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 90, 120, -1));

        jlblNomeSobrenome3.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome3.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome3.setText("Adesivo");
        jPanelCampos.add(jlblNomeSobrenome3, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, -1, -1));

        txtMarca.setForeground(new java.awt.Color(0, 102, 153));
        txtMarca.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtMarcaKeyTyped(evt);
            }
        });
        jPanelCampos.add(txtMarca, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 30, 120, -1));

        jlblNomeSobrenome4.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome4.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome4.setText("Marca(*)");
        jPanelCampos.add(jlblNomeSobrenome4, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 10, -1, -1));

        txtAdesivo.setForeground(new java.awt.Color(0, 102, 153));
        txtAdesivo.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtAdesivo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtAdesivoKeyTyped(evt);
            }
        });
        jPanelCampos.add(txtAdesivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, 90, -1));

        jlblNomeSobrenome5.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome5.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome5.setText("Cor(*)");
        jPanelCampos.add(jlblNomeSobrenome5, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 70, -1, -1));

        jlblNomeSobrenome6.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome6.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome6.setText("Modelo(*)");
        jPanelCampos.add(jlblNomeSobrenome6, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 70, -1, -1));

        jlblNomeSobrenome2.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome2.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome2.setText("(*) Campos obrigatórios");
        jPanelCampos.add(jlblNomeSobrenome2, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 130, -1, -1));

        txtModelo.setForeground(new java.awt.Color(0, 102, 153));
        txtModelo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtModeloKeyTyped(evt);
            }
        });
        jPanelCampos.add(txtModelo, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 90, 120, -1));

        jlblNomeSobrenome8.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jlblNomeSobrenome8.setForeground(new java.awt.Color(133, 133, 133));
        jlblNomeSobrenome8.setText("Placa(*)");
        jPanelCampos.add(jlblNomeSobrenome8, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 10, -1, -1));

        btnFinalizar.setBackground(new java.awt.Color(0, 102, 255));
        btnFinalizar.setFont(new java.awt.Font("Dialog", 0, 14)); // NOI18N
        btnFinalizar.setForeground(new java.awt.Color(204, 204, 204));
        btnFinalizar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnFinalizar.setText("FINALIZAR");
        btnFinalizar.setAlignmentX(0.5F);
        btnFinalizar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnFinalizar.setOpaque(true);
        btnFinalizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnFinalizarMouseClicked(evt);
            }
        });

        lblGeradoAuto.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        lblGeradoAuto.setForeground(new java.awt.Color(133, 133, 133));
        lblGeradoAuto.setText("Código gerado automaticamente");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(8, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(lblGeradoAuto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 208, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanelCampos, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jPanelCampos, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblGeradoAuto))
                .addGap(15, 15, 15))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void formAncestorRemoved(javax.swing.event.AncestorEvent evt) {//GEN-FIRST:event_formAncestorRemoved

    }//GEN-LAST:event_formAncestorRemoved

    private void btnFinalizarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnFinalizarMouseClicked
        FinalizarCadastro();
    }//GEN-LAST:event_btnFinalizarMouseClicked

    private void txtPlacaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlacaKeyReleased

    }//GEN-LAST:event_txtPlacaKeyReleased

    private void txtPlacaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlacaKeyPressed

    }//GEN-LAST:event_txtPlacaKeyPressed

    private void txtPlacaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPlacaKeyTyped
        char enter = evt.getKeyChar();
        if (!(Character.isLetterOrDigit(enter)) || txtPlaca.getText().length() > 6) {
            evt.consume();
        }
    }//GEN-LAST:event_txtPlacaKeyTyped

    private void txtCorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCorKeyTyped
        if (txtCor.getText().length() > 14) {
            evt.consume();
        }
    }//GEN-LAST:event_txtCorKeyTyped

    private void txtMarcaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtMarcaKeyTyped
        if (txtMarca.getText().length() > 14) {
            evt.consume();
        }
    }//GEN-LAST:event_txtMarcaKeyTyped

    private void txtModeloKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtModeloKeyTyped
        if (txtModelo.getText().length() > 14) {
            evt.consume();
        }
    }//GEN-LAST:event_txtModeloKeyTyped

    private void txtAdesivoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtAdesivoKeyTyped
        char enter = evt.getKeyChar();
        if (!Character.isDigit(enter) || txtAdesivo.getText().length() > 3) {
            evt.consume();
        }
    }//GEN-LAST:event_txtAdesivoKeyTyped

    public boolean isAlpha(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c)) {
                return false;
            }

        }
        return true;
    }

    public boolean isAlphaOrDigit(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetterOrDigit(c)) {
                return false;
            }
        }
        return true;
    }

    public boolean isAlphaOrSpace(String name) {
        char[] chars = name.toCharArray();

        for (char c : chars) {
            if (!Character.isLetter(c) && !Character.isWhitespace(c)) {
                return false;
            }
        }
        return true;
    }

    private void FinalizarCadastro() {

        if (txtCod.getText().equals("automatico")) {
            veiculo = new Veiculo();
        }

        veiculo.setPlaca(txtPlaca.getText());
        veiculo.setCor(txtCor.getText());
        veiculo.setMarca(txtMarca.getText());
        veiculo.setModelo(txtModelo.getText());

        if (txtAdesivo.getText().isEmpty()) {
            veiculo.setAdesivo(null);
        } else {
            veiculo.setAdesivo(txtAdesivo.getText());
        }

        if (txtCod.getText().equals("automatico")) {
            switch (veiculo.salvar()) {
                case 0:
                    JOptionPane.showConfirmDialog(null, "Cadastro realizado com sucesso!", "Cadastro de Veículo", JOptionPane.CLOSED_OPTION);
                    Component[] componentes = jPanelCampos.getComponents();
                    for (Component cmp : componentes) {
                        if (cmp instanceof JTextField) {
                            ((JTextField) cmp).setText("");
                        }
                        if (cmp instanceof JFormattedTextField) {
                            ((JFormattedTextField) cmp).setText("");
                        }
                    }
                    txtCod.setText("automatico");
                    txtPlaca.requestFocus();
                    break;
                case 1:
                    JOptionPane.showConfirmDialog(null, "Preencha todos os campos corretamente.", "Cadastro de Veículo", JOptionPane.CLOSED_OPTION);
                    break;
                case 2:
                    JOptionPane.showConfirmDialog(null, "Ocorreu um erro ao cadastrar no Banco.", "Cadastro de Veículo", JOptionPane.CLOSED_OPTION);
                    break;
            }
        } else {
            switch (veiculo.update()) {
                case 0:
                    JOptionPane.showConfirmDialog(null, "Aleração realizada com sucesso!", "Alteração de Veículo", JOptionPane.CLOSED_OPTION);
                    break;
                case 1:
                    JOptionPane.showConfirmDialog(null, "Preencha todos os campos corretamente.", "Alteração de Veículo", JOptionPane.CLOSED_OPTION);
                    break;
                case 2:
                    JOptionPane.showConfirmDialog(null, "Ocorreu um erro ao Alterar no Banco.", "Alteração de Veículo", JOptionPane.CLOSED_OPTION);
                    break;
            }

        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel btnFinalizar;
    private javax.swing.JPanel jPanelCampos;
    private javax.swing.JLabel jlblNomeSobrenome1;
    private javax.swing.JLabel jlblNomeSobrenome2;
    private javax.swing.JLabel jlblNomeSobrenome3;
    private javax.swing.JLabel jlblNomeSobrenome4;
    private javax.swing.JLabel jlblNomeSobrenome5;
    private javax.swing.JLabel jlblNomeSobrenome6;
    private javax.swing.JLabel jlblNomeSobrenome8;
    private javax.swing.JLabel lblGeradoAuto;
    private javax.swing.JTextField txtAdesivo;
    private javax.swing.JTextField txtCod;
    private javax.swing.JTextField txtCor;
    private javax.swing.JTextField txtMarca;
    private javax.swing.JTextField txtModelo;
    private javax.swing.JTextField txtPlaca;
    // End of variables declaration//GEN-END:variables

}
