package projetoestacionamento.janelas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.util.ArrayList;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import modal.Motorista;

/**
 *
 * @author santucci
 */
public class MotoristasGUI extends javax.swing.JPanel {

    ArrayList<String> criterios = new ArrayList();
    DefaultTableModel model;
    ArrayList<Motorista> motoristas;

    public MotoristasGUI() {
        initComponents();

        Tabela.getTableHeader().setDefaultRenderer(new HeaderColor());
        Tabela.setShowGrid(false);
        model = new DefaultTableModel();
        model.addColumn("RG");
        model.addColumn("Nome");
        model.addColumn("Sexo");
        model.addColumn("Telefone");
        model.addColumn("Celular");
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        Tabela = new javax.swing.JTable();
        btnRemover = new javax.swing.JLabel();
        btnPesquisar = new javax.swing.JLabel();
        btnModificar = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtRG = new javax.swing.JTextField();
        txtCelular = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtNome = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtTelefone = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(238, 238, 238));
        setMaximumSize(new java.awt.Dimension(839, 464));
        setMinimumSize(new java.awt.Dimension(839, 464));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Tabela.setBackground(new java.awt.Color(238, 238, 238));
        Tabela.setForeground(new java.awt.Color(102, 102, 102));
        Tabela.setGridColor(new java.awt.Color(238, 238, 238));
        Tabela.setOpaque(false);
        Tabela.setSelectionBackground(new java.awt.Color(51, 51, 51));
        jScrollPane1.setViewportView(Tabela);
        Tabela.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        if (Tabela.getColumnModel().getColumnCount() > 0) {
            Tabela.getColumnModel().getColumn(0).setPreferredWidth(35);
            Tabela.getColumnModel().getColumn(1).setPreferredWidth(100);
            Tabela.getColumnModel().getColumn(2).setPreferredWidth(20);
        }

        add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(17, 67, 810, 330));

        btnRemover.setBackground(new java.awt.Color(0, 102, 255));
        btnRemover.setForeground(new java.awt.Color(255, 255, 255));
        btnRemover.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnRemover.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/remove.png"))); // NOI18N
        btnRemover.setText("REMOVER");
        btnRemover.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRemover.setIconTextGap(10);
        btnRemover.setOpaque(true);
        btnRemover.setPreferredSize(new java.awt.Dimension(80, 40));
        btnRemover.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRemoverMouseClicked(evt);
            }
        });
        add(btnRemover, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 410, 110, -1));

        btnPesquisar.setBackground(new java.awt.Color(0, 102, 255));
        btnPesquisar.setForeground(new java.awt.Color(255, 255, 255));
        btnPesquisar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnPesquisar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/musica-searcher.png"))); // NOI18N
        btnPesquisar.setText("PESQUISAR");
        btnPesquisar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnPesquisar.setIconTextGap(10);
        btnPesquisar.setOpaque(true);
        btnPesquisar.setPreferredSize(new java.awt.Dimension(80, 40));
        btnPesquisar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnPesquisarMouseClicked(evt);
            }
        });
        add(btnPesquisar, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 30, 120, 30));

        btnModificar.setBackground(new java.awt.Color(0, 102, 255));
        btnModificar.setForeground(new java.awt.Color(255, 255, 255));
        btnModificar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/edit.png"))); // NOI18N
        btnModificar.setText("MODIFICAR");
        btnModificar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnModificar.setIconTextGap(10);
        btnModificar.setOpaque(true);
        btnModificar.setPreferredSize(new java.awt.Dimension(80, 40));
        btnModificar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnModificarMouseClicked(evt);
            }
        });
        add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 410, 110, -1));

        jLabel4.setBackground(new java.awt.Color(0, 102, 255));
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/addButton16.png"))); // NOI18N
        jLabel4.setText("NOVO");
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel4.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        jLabel4.setIconTextGap(10);
        jLabel4.setOpaque(true);
        jLabel4.setPreferredSize(new java.awt.Dimension(80, 40));
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });
        add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 410, 80, -1));

        jLabel5.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(133, 133, 133));
        jLabel5.setText("CELULAR");
        add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 10, -1, -1));

        txtRG.setForeground(new java.awt.Color(0, 102, 153));
        add(txtRG, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, 110, -1));

        txtCelular.setForeground(new java.awt.Color(0, 102, 153));
        add(txtCelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 30, 110, -1));

        jLabel6.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(133, 133, 133));
        jLabel6.setText("RG");
        add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        txtNome.setForeground(new java.awt.Color(0, 102, 153));
        add(txtNome, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 30, 110, -1));

        jLabel7.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(133, 133, 133));
        jLabel7.setText("NOME");
        add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 10, -1, -1));

        txtTelefone.setForeground(new java.awt.Color(0, 102, 153));
        add(txtTelefone, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 30, 110, -1));

        jLabel8.setFont(new java.awt.Font("Dialog", 0, 10)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(133, 133, 133));
        jLabel8.setText("TELEFONE");
        add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(280, 10, -1, -1));

        jLabel1.setBackground(new java.awt.Color(0, 102, 255));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/projetoestacionamento/imgs/pesquisaassociativa16.png"))); // NOI18N
        jLabel1.setText("ASSOCIAR AO VEÍCULO");
        jLabel1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel1.setIconTextGap(10);
        jLabel1.setOpaque(true);
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel1MouseClicked(evt);
            }
        });
        add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 410, 190, 40));

        getAccessibleContext().setAccessibleName("");
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        MotoristaGUI cadTela = new MotoristaGUI();
        JDialog dialogCad = new JDialog();
        dialogCad.setTitle("Novo Motorista");
        dialogCad.setModal(true);
        //dialogCad.setLocationRelativeTo(this);

        dialogCad.getContentPane().add(cadTela, BorderLayout.CENTER);
        dialogCad.pack();
        dialogCad.setLocationRelativeTo(null);
        dialogCad.setSize(821, 315);
        //dialogCad.setUndecorated(true);
        //dialogCad.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        dialogCad.setResizable(false);
        dialogCad.setVisible(true);

    }//GEN-LAST:event_jLabel4MouseClicked

    private void btnModificarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnModificarMouseClicked
        System.out.println(Tabela.isFocusOwner());
        if (Tabela.isFocusOwner()) {
            MotoristaGUI cadTela = new MotoristaGUI(motoristas.get(Tabela.getSelectedRow()));
            JDialog dialogCad = new JDialog();
            dialogCad.setTitle("Modificar Motorista");
            dialogCad.setModal(true);

            dialogCad.getContentPane().add(cadTela, BorderLayout.CENTER);
            dialogCad.pack();
            dialogCad.setLocationRelativeTo(null);
            dialogCad.setSize(821, 315);
            dialogCad.setResizable(false);
            dialogCad.setVisible(true);
        } else {
            JOptionPane.showConfirmDialog(null, "Você precisa selecionar algum item da tabela.", "Alteração de motorista", JOptionPane.CLOSED_OPTION);
        }
    }//GEN-LAST:event_btnModificarMouseClicked

    private void btnPesquisarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnPesquisarMouseClicked
        criterios.clear();

        if (txtRG.getText().length() > 0) {
            criterios.add("MotRG LIKE '%" + txtRG.getText() + "%'");
        }
        if (txtNome.getText().length() > 0) {
            criterios.add("MotNomeCompleto LIKE '%" + txtNome.getText() + "%'");
        }
        if (txtTelefone.getText().length() > 0) {
            criterios.add("MotTelefone LIKE '%" + txtTelefone.getText() + "%'");
        }
        if (txtCelular.getText().length() > 0) {
            criterios.add("MotTelefone2 LIKE '%" + txtCelular.getText() + "%'");
        }

        if (criterios.size() > 0) {
            motoristas = Motorista.SelectPorCriterio(criterios);
        } else {
            motoristas = Motorista.SelectAll();
        }

        while (model.getRowCount() > 0) {
            model.removeRow(0);
        }

        for (Motorista dado : motoristas) {
            model.addRow(new Object[]{
                dado.getRg(),
                dado.getNomeCompleto(),
                dado.getSexo(),
                dado.getTelPrincipal(),
                dado.getTelRecados()
            });
        }
        Tabela.setModel(model);
    }//GEN-LAST:event_btnPesquisarMouseClicked

    private void btnRemoverMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRemoverMouseClicked
        if (Tabela.isFocusOwner()) {
            int selected = JOptionPane.showConfirmDialog(null, "Você realmente deseja remover o Motorista: " + (String) Tabela.getValueAt(Tabela.getSelectedRow(), 1) + " ?", "Remoção de motorista", JOptionPane.YES_NO_OPTION);
            if (selected == JOptionPane.YES_OPTION) {
                Motorista.RemoverPorRG((String) Tabela.getValueAt(Tabela.getSelectedRow(), 0));
                model.removeRow(Tabela.getSelectedRow());
            }
        } else {
            JOptionPane.showConfirmDialog(null, "Você precisa selecionar algum item da tabela.", "Remoção de motorista", JOptionPane.CLOSED_OPTION);
        }

    }//GEN-LAST:event_btnRemoverMouseClicked

    private void jLabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseClicked
        if (Tabela.isFocusOwner()) {
            AssociacaoGUI associacaoTela = new AssociacaoGUI(motoristas.get(Tabela.getSelectedRow()));
            JDialog dialogCad = new JDialog();
            dialogCad.setTitle("Associar aos Veículos");
            dialogCad.setModal(true);

            dialogCad.getContentPane().add(associacaoTela, BorderLayout.CENTER);
            dialogCad.pack();
            dialogCad.setLocationRelativeTo(null);
            dialogCad.setSize(591, 385);
            dialogCad.setResizable(false);
            dialogCad.setVisible(true);
        } else {
            JOptionPane.showConfirmDialog(null, "Você precisa selecionar algum item da tabela.", "Associação de motorista", JOptionPane.CLOSED_OPTION);
        }
    }//GEN-LAST:event_jLabel1MouseClicked

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTable Tabela;
    private javax.swing.JLabel btnModificar;
    private javax.swing.JLabel btnPesquisar;
    private javax.swing.JLabel btnRemover;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField txtCelular;
    private javax.swing.JTextField txtNome;
    private javax.swing.JTextField txtRG;
    private javax.swing.JTextField txtTelefone;
    // End of variables declaration//GEN-END:variables

    //Utilizado para alterar o header da tabela
    public static class HeaderColor extends DefaultTableCellRenderer {

        public HeaderColor() {
            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent(JTable mytable, Object value, boolean selected, boolean focused, int row, int column) {
            super.getTableCellRendererComponent(mytable, value, selected, focused, row, column);
            setBackground(new Color(0, 102, 255));
            setForeground(Color.white);
            return this;
        }
    }
}
