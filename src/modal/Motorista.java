/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modal;

import java.util.ArrayList;

/**
 *
 * @author santucci
 */
public class Motorista {

    public final int CONCLUIDO = 0;
    public final int ERROR_VALIDACAO = 1;
    public final int ERROR_BANCO = 2;

    private String rg;
    private String nomeCompleto = null;
    private String sexo;
    private String foto64;
    private String telPrincipal = null;
    private String telRecados = null;
    private ArrayList<Veiculo> veiculos = new ArrayList();

    public Motorista(String rg, String nomeCompleto, String sexo, String foto64, String telPrincipal, String telRecados) {
        this.rg = rg;
        this.nomeCompleto = nomeCompleto;
        this.sexo = sexo;
        this.foto64 = foto64;
        this.telPrincipal = telPrincipal;
        this.telRecados = telRecados;
    }

    public Motorista() {
    }

    public String getRg() {
        return rg;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nome) {
        this.nomeCompleto = nome;
    }

    public String getFoto64() {
        return foto64;
    }

    public void setFoto64(String foto64) {
        this.foto64 = foto64;
    }

    public String getTelPrincipal() {
        return telPrincipal;
    }

    public void setTelPrincipal(String telPrincipal) {
        this.telPrincipal = telPrincipal;
    }

    public String getTelRecados() {
        return telRecados;
    }

    public void setTelRecados(String telRecados) {
        this.telRecados = telRecados;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public ArrayList<Veiculo> getVeiculos() {
        return veiculos;
    }

    public int salvar() {
        if (validarDados()) {
            String insertion = "INSERT INTO Motorista(MotRG, MotNomeCompleto, MotSexo,  MotFoto, MotTelefone, MotTelefone2) VALUES"
                    + "(@MotRG, @MotNome, @MotSexo, @MotFoto, @MotTelefone, @MotCelular)";
            insertion = insertion.replaceAll("@MotRG", "'" + this.rg + "'");
            insertion = insertion.replaceAll("@MotNome", "'" + this.nomeCompleto + "'");
            insertion = insertion.replaceAll("@MotSexo", "'" + this.sexo + "'");
            insertion = insertion.replaceAll("@MotFoto", "'" + this.foto64 + "'");
            if (this.telPrincipal == null) {
                insertion = insertion.replaceAll("@MotTelefone", "NULL");
            } else {
                insertion = insertion.replaceAll("@MotTelefone", "'" + this.telPrincipal + "'");
            }

            if (this.telRecados == null) {
                insertion = insertion.replaceAll("@MotCelular", "NULL");
            } else {
                insertion = insertion.replaceAll("@MotCelular", "'" + this.telRecados + "'");
            }

            BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");

            if (!db.inserir(insertion)) {
                return ERROR_BANCO;
            }

        } else {
            return ERROR_VALIDACAO;
        }
        return CONCLUIDO;
    }

    public static ArrayList<Motorista> SelectAll() {
        ArrayList<Motorista> Motoristas = new ArrayList();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        ArrayList<String[]> dados = db.selection("Select * FROM Motorista");

        for (int i = 0; i < dados.size(); i++) {
            Motoristas.add(new Motorista(((String[]) dados.get(i))[0],
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    ((String[]) dados.get(i))[5]
            ));
        }
        return Motoristas;
    }

    public static ArrayList<Motorista> SelectPorCriterio(ArrayList<String> criterios) {
        ArrayList<Motorista> Motoristas = new ArrayList();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        String select = "Select * FROM Motorista WHERE ";

        for (String criterio : criterios) {
            select += criterio + " AND ";
        }
        select = select.substring(0, select.length() - 4);
        ArrayList<String[]> dados = db.selection(select);

        System.out.println(select);

        for (int i = 0; i < dados.size(); i++) {
            Motoristas.add(new Motorista(((String[]) dados.get(i))[0],
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    ((String[]) dados.get(i))[5]
            ));
        }
        return Motoristas;
    }

    public static boolean RemoverPorRG(String rg) {
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        return db.delete("DELETE FROM Motorista WHERE MotRG = '" + rg + "'");
    }

    public int update() {
        if (validarDados()) {
            String updateQuery = "UPDATE Motorista SET "
                    + " MotNomeCompleto = @MotNome,"
                    + " MotSexo = @MotSexo, "
                    + " MotFoto = @MotFoto,"
                    + " MotTelefone = @MotTelefone,"
                    + " MotTelefone2 = @MotCelular"
                    + " WHERE MotRG = @MotRG";
            updateQuery = updateQuery.replaceAll("@MotRG", "'" + this.rg + "'");
            updateQuery = updateQuery.replaceAll("@MotNome", "'" + this.nomeCompleto + "'");
            updateQuery = updateQuery.replaceAll("@MotSexo", "'" + this.sexo + "'");
            updateQuery = updateQuery.replaceAll("@MotFoto", "'" + this.foto64 + "'");
            if (this.telPrincipal == null) {
                updateQuery = updateQuery.replaceAll("@MotTelefone", "NULL");
            } else {
                updateQuery = updateQuery.replaceAll("@MotTelefone", "'" + this.telPrincipal + "'");
            }

            if (this.telRecados == null) {
                updateQuery = updateQuery.replaceAll("@MotCelular", "NULL");
            } else {
                updateQuery = updateQuery.replaceAll("@MotCelular", "'" + this.telRecados + "'");
            }

            BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
            if (!db.update(updateQuery)) {
                return ERROR_BANCO;
            }
        } else {
            return ERROR_VALIDACAO;
        }
        return CONCLUIDO;
    }

    public boolean validarDados() {

        boolean saida = true;

        if (this.foto64 == null) {
            saida = false;
        }

        if (this.nomeCompleto == null) {
            saida = false;
        } else {
            if (this.nomeCompleto.length() < 8) {
                saida = false;
            }
        }

        if (this.rg != null) {
            if (this.rg.length() != 12) {
                saida = false;
            }
        }

        if (this.telPrincipal != null) {
            if (this.telPrincipal.length() <= 8) {
                saida = false;
            }
        }

        if (this.telRecados != null) {
            if (this.telRecados.length() <= 8) {
                saida = false;
            }
        }
        return saida;
    }

    public int adicionarVeiculo(Veiculo veiculo) {
        String insertion = "INSERT INTO Motorista_Veiculo(MotRG,  VeiCod) VALUES"
                + "(@MotRG, @VeiCod)";
        insertion = insertion.replaceAll("@MotRG", "'" + this.rg + "'");
        insertion = insertion.replaceAll("@VeiCod", String.valueOf(veiculo.getCodigo()));

        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");

        if (!db.inserir(insertion)) {
            return ERROR_BANCO;
        }

        veiculos.add(veiculo);
        return CONCLUIDO;
    }

    public boolean removerVeiculo(int idVeiculo) {
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        if (db.delete("DELETE FROM Motorista_Veiculo WHERE MotRG = '" + this.rg + "' AND VeiCod = " + idVeiculo)) {
            System.out.println("DELETE FROM Motorista_Veiculo WHERE MotRG = '" + this.rg + "' AND VeiCod = " + idVeiculo);
            for (Veiculo veiculo : veiculos) {
                if (veiculo.getCodigo() == idVeiculo) {
                    System.out.println("VEICULOS REMOVIDOS:");
                    System.out.println(veiculo.toString());
                    veiculos.remove(veiculo);
                    break;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    public void atualizarVeiculos() {
        veiculos.clear();
        BancoDeDados db = new BancoDeDados("jdbc:sqlserver://localhost:1433;", "BancoPark");
        ArrayList<String[]> dados = db.selection("SELECT Veiculo.VeiCod, "
                + "Veiculo.VeiPlaca, "
                + "Veiculo.VeiMarca, "
                + "Veiculo.VeiModelo, "
                + "Veiculo.VeiCor, "
                + "Veiculo.VeiAdesivo "
                + "FROM Motorista_Veiculo "
                + "INNER JOIN Veiculo ON Motorista_Veiculo.VeiCod = Veiculo.VeiCod "
                + "WHERE Motorista_Veiculo.MotRG = '" + this.rg + "'");

        for (int i = 0; i < dados.size(); i++) {
            veiculos.add(new Veiculo(Integer.parseInt(((String[]) dados.get(i))[0]),
                    ((String[]) dados.get(i))[1],
                    ((String[]) dados.get(i))[2],
                    ((String[]) dados.get(i))[3],
                    ((String[]) dados.get(i))[4],
                    (((String[]) dados.get(i))[5])
            ));
        }
    }

    @Override
    public String toString() {
        return "Motorista{" + "rg=" + rg + ", nomeCompleto=" + nomeCompleto + ", sexo=" + sexo + ", foto64=" + foto64 + ", telPrincipal=" + telPrincipal + ", telRecados=" + telRecados + ", veiculos=" + veiculos + '}';
    }

}
