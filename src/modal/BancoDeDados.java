/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modal;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import com.microsoft.sqlserver.jdbc.SQLServerStatement;
import java.util.ArrayList;

/**
 *
 * @author santucci
 */
public class BancoDeDados {

    private String urlConex = "jdbc:sqlserver://localhost:1433;";
    private Connection conn;

    public BancoDeDados(String conex, String db) {
        this.urlConex = conex;
        /*try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
        }*/

        try {
            conn = DriverManager.getConnection(urlConex + "databaseName=" + db + ";" + "+IntegratedSecurity=true;", "sa", "12345Root");
        } catch (SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    BancoDeDados() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void closeConnection() {
        try {
            conn.close();
        } catch (SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void setUrlConex(String urlConex) {
        this.urlConex = urlConex;
        try {
            conn = DriverManager.getConnection(urlConex, "", "");
        } catch (SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean inserir(String insert) {
        try {
            Statement st = conn.createStatement();
            st.executeUpdate(insert);
            closeConnection();
            return true;

        } catch (SQLException e) {
            System.err.println("Got an exception! ");
            System.err.println(e.getMessage());
            closeConnection();
            return false;
        }
    }

    public ArrayList<String[]> selection(String select) {
        ArrayList<String[]> dados = new ArrayList();
        try {
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(select);
            ResultSetMetaData rsmd = rs.getMetaData();
            int maxcol = rsmd.getColumnCount();
            String[] coluna;
            System.out.println("Quantidade max de colunas:" + maxcol);
            int linhas = 0;
            while (rs.next()) {
                coluna = new String[maxcol];
                for (int i = 0; i < maxcol; i++) {
                    coluna[i] = rs.getString(i + 1);
                }
                dados.add(coluna);
                linhas++;
            }
            System.out.println("Linhas Encontradas:" + linhas);
        } catch (SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
            dados = null;
        }
        closeConnection();
        return dados;
    }

    public boolean delete(String query) {
        try {
            PreparedStatement st = conn.prepareStatement(query);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
            closeConnection();
            return false;
        }
        closeConnection();
        return true;
    }

    public boolean update(String query) {
        try {
            PreparedStatement st = conn.prepareStatement(query);
            st.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(BancoDeDados.class.getName()).log(Level.SEVERE, null, ex);
            closeConnection();
            return false;
        }
        closeConnection();
        return true;
    }
}
