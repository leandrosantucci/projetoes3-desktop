/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modal;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

/**
 *
 * @author santucci
 */
public class Conversao64 {

    public static String ImagePNGtoBase64(File arquivo) {
        String saida64 = null;
        ByteArrayOutputStream conjuntoBytes = new ByteArrayOutputStream();
        try {
            BufferedImage image = ImageIO.read(arquivo);

            ImageIO.write(image, "png", conjuntoBytes);
            byte[] imageInByte = conjuntoBytes.toByteArray();
            saida64 = Base64.getEncoder().encodeToString(imageInByte);
        } catch (IOException ex) {
            Logger.getLogger(Conversao64.class.getName()).log(Level.SEVERE, null, ex);
        }
        return saida64;
    }

    public static ImageIcon base64toImageIcon(String base64, int Width, int Height) {
        BufferedImage image = null;
        Image img = null;
        byte[] decoded = Base64.getDecoder().decode(base64.getBytes());
        try {
            image = ImageIO.read(new ByteArrayInputStream(decoded));
            img = image.getScaledInstance(Width, Height, Image.SCALE_SMOOTH);
        } catch (IOException ex) {
            Logger.getLogger(Conversao64.class.getName()).log(Level.SEVERE, null, ex);
        }
        return new ImageIcon(img);
    }
}
